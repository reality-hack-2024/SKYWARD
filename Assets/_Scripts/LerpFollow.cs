using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpFollow : MonoBehaviour
{
    [SerializeField] private GameObject target;
    [SerializeField] private float lerpAmount;
    [SerializeField] private LineRenderer renderer;
    private Vector3 offset;
    void Start()
    {
        offset = transform.position - target.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        var targetPos = target.transform.position + offset;
        transform.position = Vector3.Lerp(transform.position, targetPos, lerpAmount * Time.deltaTime);
        renderer.SetPosition(0, renderer.transform.position);
        renderer.SetPosition(1, target.transform.position);
    }
}
