using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class AnimationManager : MonoBehaviour
{
    [SerializeField] private Animator planeAnim;
    [SerializeField] private GameObject fireFx;
    [SerializeField] private GameObject smokeFx;
    
    private int testScript = 0;
    int currScriptIndex = 0;
    public void TestNext()
    {
        testScript++;
        OnNext(testScript);
    }
    
    void Awake()
    {
        StateManager.OnNextScript += OnNext;
        PlaneAnimator.OnDroneHit += OnDroneHit;
        VideoManager.VideoEnded += VideoEnded;
        smokeFx.SetActive(false);
        fireFx.SetActive(false);
    }

    private void VideoEnded()
    {
        print("end");
        if (currScriptIndex > 1)
            planeAnim.speed = 0.15f;
        print(planeAnim.speed);
    }

    private void OnDestroy()
    {
        StateManager.OnNextScript -= OnNext;
        PlaneAnimator.OnDroneHit -= OnDroneHit;
        VideoManager.VideoEnded -= VideoEnded;
    }

    private void Start()
    {
        TurbulenceEvent();
    }

    void OnNext(int scriptNum)
    {
        currScriptIndex = scriptNum;
        planeAnim.speed = 1;
        print(scriptNum);
        switch (scriptNum)
        {
            case 1:
                BankLeftEvent();
                break;
            case 3:
                SmokeGrowEvent();
                break;
            case 4:
                DescentEvent();
                break;
            case 5:
                LandingGearEvent();
                break;
            case 6:
                LandingEvent();
                break;
        }
    }

    void TurbulenceEvent()
    {
        
    }

    void BankLeftEvent()
    {
        planeAnim.SetTrigger("BankLeft");
    }

    private void OnDroneHit()
    {
        DroneEvent();
    }
    
    void DroneEvent()
    {
        StartCoroutine(DroneEventCoroutine());
    }

    IEnumerator DroneEventCoroutine()
    {
        yield return new WaitForSeconds(0.5f);
        smokeFx.SetActive(true);
        fireFx.SetActive(true);
    }
    private void SmokeGrowEvent()
    {
    }

    void DescentEvent()
    {
        planeAnim.SetTrigger("Landing");
    }

    void LandingGearEvent()
    {
        
    }
    
    void LandingEvent()
    {
        
    }
}
