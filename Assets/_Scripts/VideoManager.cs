using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoManager : MonoBehaviour
{
    [SerializeField] private List<VideoClip> videos;
    [SerializeField] private VideoClip loopVideo1, loopVideo2;
    [SerializeField] private VideoPlayer videoPlayer;

    public static event Action VideoEnded;
    
    private int currScriptIndex;
    
    void Awake()
    {
        StateManager.OnNextScript += OnNext;
        videoPlayer.loopPointReached += OnVideoEnd;
    }

    private void OnDestroy()
    {
        StateManager.OnNextScript -= OnNext;
    }

    void OnNext(int scriptNum)
    {
        if (scriptNum >= 0 && scriptNum < videos.Count)
        {
            currScriptIndex = scriptNum;
            videoPlayer.clip = videos[scriptNum];
            videoPlayer.Play();
        }
    }

    void OnVideoEnd(VideoPlayer source)
    {
        if (currScriptIndex < 2 || currScriptIndex > 4)
        {
            source.clip = loopVideo1;
        }
        else
        {
            source.clip = loopVideo2;
        }
        source.Play();
        VideoEnded?.Invoke();
    }
}
