using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneAnimator : MonoBehaviour
{
    public static event Action OnDroneHit;
    public void HandleDroneEvent()
    {
        OnDroneHit?.Invoke();
    }
}
