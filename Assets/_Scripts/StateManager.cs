using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StateManager : MonoBehaviour
{
    public int scriptNumber = 0;

    public List<string> pilotScripts = new List<string>();
    public List<string> atcScripts = new List<string>();

    public TextMeshProUGUI pilotDisplayText;
    public TextMeshProUGUI atcDisplayText;

    public static event Action<int> OnNextScript;

    private void Awake()
    {
        PlaneAnimator.OnDroneHit += onDroneHit;
    }

    private void Start()
    {
        setDisplayTexts();
    }

    private void OnDestroy()
    {
        PlaneAnimator.OnDroneHit -= onDroneHit;
    }

    public void onDroneHit()
    {
        print("drone hit!");
        nextScript();
    }
    
    public void nextScript()
    {
        if (scriptNumber < pilotScripts.Count) {
            scriptNumber++;
            OnNextScript?.Invoke(scriptNumber);
            setDisplayTexts();
        }
    }

    public void setDisplayTexts()
    {
        if (scriptNumber < pilotScripts.Count)
        {
            pilotDisplayText.text = pilotScripts[scriptNumber];
            atcDisplayText.text = atcScripts[scriptNumber];
        }
    }

}
