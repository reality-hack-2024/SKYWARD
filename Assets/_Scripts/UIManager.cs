using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public enum Layer
    {
        Objects,
        Weather,
        Airfield
    }
    [SerializeField] private GameObject objectsLayer, weatherLayer, airfieldLayer;

    private bool weatherVisible, windVisible, objectsVisible;
    private void Start()
    {
        weatherVisible = true;
        windVisible = false;
        objectsVisible = true;
        UpdateLayers();
    }

    public void ToggleWeather()
    {
        weatherVisible = !weatherVisible;
        UpdateLayers();
    }

    public void ToggleWind()
    {
        windVisible = !windVisible;
        UpdateLayers();
    }

    public void ToggleObjects()
    {
        objectsVisible = !objectsVisible;
        UpdateLayers();
    }

    void UpdateLayers()
    {
        weatherLayer.SetActive(weatherVisible);
        airfieldLayer.SetActive(windVisible);
        objectsLayer.SetActive(objectsVisible);
    }
}
