using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ButtonInteract : MonoBehaviour
{
    // UnityEvent to be called when button is poked.
    //public UnityEvent onPoke;

    /*
    private void OnTriggerEnter(Collider other)
    {
        // Check if the collider is the finger or controller that should trigger the button
        if (other.CompareTag("FingerTag")) // Make sure the finger or controller has the correct tag
        {
            // Trigger whatever action you want here. 
            // For example, call a UnityEvent that can be set from the editor.
            onPoke.Invoke();
        }
    }
    */

    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("BUTTON IS PRESSED");
    }
}
