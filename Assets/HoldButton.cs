using System.Collections;
using System.Collections.Generic;
using Oculus.Interaction;
using UnityEngine;

public class HoldButton : MonoBehaviour
{
    [SerializeField] private StateManager sMan;
    private float timeClicked = 0;
    [SerializeField] private float thresh;
    [SerializeField] private PokeInteractable poke;
    private int currScriptIndex = 0;
    void Start()
    {
        VideoManager.VideoEnded += VideoManagerOnVideoEnded;
        StateManager.OnNextScript += StateManagerOnOnNextScript;
        poke.enabled = false;
    }

    private void StateManagerOnOnNextScript(int obj)
    {
        currScriptIndex++;
        poke.enabled = false;
    }

    private void VideoManagerOnVideoEnded()
    {
        if (currScriptIndex != 1)
            poke.enabled = true;
    }

    public void OnBtnClicked()
    {
        timeClicked = Time.time;
    }

    public void OnBtnEnd()
    {
        if (Time.time - timeClicked > thresh)
        {
            sMan.nextScript();
        }
    }
    void Update()
    {
    }
}
